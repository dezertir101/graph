package graph;

import graph.component.Edge;
import graph.component.Vertex;
import javafx.util.Pair;

import java.security.InvalidParameterException;
import java.util.*;

/**
 * Created by andr1115 on 10.05.2017.
 */
abstract class AbstractGraph implements GraphInterface{

    private LinkedList<Edge> path = new LinkedList<>();

    private List<Vertex> vertexList = new ArrayList<>();

    private HashMap<Pair<Vertex,Vertex>,Edge> vertexEdgeMap = new HashMap<>();

    boolean finalFound = false;

    public AbstractGraph addVertex(Vertex vertex) {
        vertexList.add(vertex);
        return this;
    }

    public LinkedList<Edge> getPath(Vertex vertexFrom, Vertex vertexTo) {
        setFinalFound(false);
        clearPath();
        discover(vertexFrom,vertexTo);
        if(!isFinalFound()){
            clearPath();
        }
        return getPath();
    }

    public AbstractGraph addEdge(Edge edge, Vertex vertexFrom, Vertex vertexTo) {

        if(Objects.isNull(vertexFrom) || Objects.isNull(vertexTo)){
            throw new InvalidParameterException("Vertex cannot be null");
        }

        if(!vertexList.contains(vertexFrom)){
            vertexList.add(vertexFrom);
        }
        if(!vertexList.contains(vertexTo)){
            vertexList.add(vertexTo);
        }

        getVertexEdgeMap().put(new Pair<>(vertexFrom,vertexTo),edge);
        return this;
    }


    public HashMap<Pair<Vertex,Vertex>,Edge> getVertexEdgeMap() {
        return vertexEdgeMap;
    }

    public AbstractGraph clearGraph(){
        this.vertexEdgeMap = new HashMap<>();
        return this;
    }

    public Vertex getVertexByName(String vertexName){
        Vertex result = null;
        for (Vertex vertex : vertexList){
            if(vertex.getName().equals(vertexName)){
                result = vertex;
                break;
            }
        }
        return result;
    }

    public List<Vertex> getVertexList() {
        return vertexList;
    }

    public boolean isFinalFound() {
        return finalFound;
    }

    public void setFinalFound(boolean finalFound) {
        this.finalFound = finalFound;
    }

    public LinkedList<Edge> getPath() {
        return path;
    }

    public void addToPath(Edge edge){
        this.path.add(edge);
    }

    protected void clearPath(){
        this.path = new LinkedList<>();
        this.vertexList.stream().forEach(vertex -> vertex.setVisited(false));
    }

    protected abstract void discover(Vertex from, Vertex to);
}
