package graph;

import graph.component.Edge;
import graph.component.Vertex;

import java.util.LinkedList;

/**
 * Created by andr1115 on 10.05.2017.
 */
interface GraphInterface {

    public <T extends AbstractGraph> T addEdge(Edge edge, Vertex vertexFrom, Vertex vertexTo);

    public <T extends AbstractGraph> T addVertex(Vertex vertex);

    public <T extends AbstractGraph> T clearGraph();

    public LinkedList<Edge> getPath(Vertex vertexFrom, Vertex vertexTo);
}
