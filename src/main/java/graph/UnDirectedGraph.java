package graph;

import graph.component.Edge;
import graph.component.Vertex;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by andr1115 on 10.05.2017.
 */
public class UnDirectedGraph extends AbstractGraph {

    protected void discover(Vertex vertex, Vertex finalVertex){
        if(!this.isFinalFound()){
            if(finalVertex.getName().equals(vertex.getName())){
                this.setFinalFound(true);
                return;
            }
            vertex.setVisited(true);
            for(Map.Entry<Pair<Vertex,Vertex>,Edge> entry : getVertexEdgeMap().entrySet()){
                Pair<Vertex,Vertex> pair = entry.getKey();
                if(pair.getKey().getName().equals(vertex.getName()) && !pair.getValue().isVisited()){
                    discover(pair.getValue(),finalVertex);
                    addToPath(entry.getValue());
                }
                if(pair.getValue().getName().equals(vertex.getName()) && !pair.getKey().isVisited()){
                    discover(pair.getKey(),finalVertex);
                    addToPath(entry.getValue());
                }
            }
        }
    }
}
