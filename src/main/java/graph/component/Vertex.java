package graph.component;

/**
 * Created by andr1115 on 03.05.2017.
 */
public class Vertex {

    private String name;

    private boolean visited = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vertex(String name) {
        this.name = name;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}
