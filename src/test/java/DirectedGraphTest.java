import graph.DirectedGraph;
import graph.component.Edge;
import graph.component.Vertex;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;


import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by Администратор on 14.05.2017.
 */
public class DirectedGraphTest{



    @Test
    public void testCreation(){
        DirectedGraph directedGraph = new DirectedGraph();

        /* Adding vertex to graph **/
        directedGraph.addVertex(new Vertex("firstVertex"));
        directedGraph.addVertex(new Vertex("secondVertex"));
        directedGraph.addVertex(new Vertex("thirdVertex"));
        directedGraph.addVertex(new Vertex("fourthVertex"));
        directedGraph.addVertex(new Vertex("fifthVertex"));
        directedGraph.addVertex(new Vertex("sixVertex"));


        directedGraph.addEdge(new Edge(),directedGraph.getVertexByName("firstVertex"),directedGraph.getVertexByName("secondVertex"));
        directedGraph.addEdge(new Edge(),directedGraph.getVertexByName("firstVertex"),directedGraph.getVertexByName("fourthVertex"));
        directedGraph.addEdge(new Edge(),directedGraph.getVertexByName("secondVertex"),directedGraph.getVertexByName("thirdVertex"));
        directedGraph.addEdge(new Edge(),directedGraph.getVertexByName("thirdVertex"),directedGraph.getVertexByName("fourthVertex"));
        directedGraph.addEdge(new Edge(),directedGraph.getVertexByName("fourthVertex"),directedGraph.getVertexByName("fifthVertex"));
        directedGraph.addEdge(new Edge(),directedGraph.getVertexByName("sixVertex"),directedGraph.getVertexByName("fifthVertex"));


        assertTrue(!directedGraph.getPath(directedGraph.getVertexByName("firstVertex"),directedGraph.getVertexByName("fifthVertex")).isEmpty());
        assertTrue(!directedGraph.getPath(directedGraph.getVertexByName("sixVertex"),directedGraph.getVertexByName("fifthVertex")).isEmpty());
        assertTrue(directedGraph.getPath(directedGraph.getVertexByName("fifthVertex"),directedGraph.getVertexByName("sixVertex")).isEmpty());
    }
}
