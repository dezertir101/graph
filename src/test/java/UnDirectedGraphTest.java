import graph.UnDirectedGraph;
import graph.component.Edge;
import graph.component.Vertex;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by Администратор on 14.05.2017.
 */
public class UnDirectedGraphTest {

    @Test
    public void testCreation(){
        UnDirectedGraph undirectedGraph = new UnDirectedGraph();

        /* Adding vertex to graph **/
        undirectedGraph.addVertex(new Vertex("firstVertex"));
        undirectedGraph.addVertex(new Vertex("secondVertex"));
        undirectedGraph.addVertex(new Vertex("thirdVertex"));
        undirectedGraph.addVertex(new Vertex("fourthVertex"));
        undirectedGraph.addVertex(new Vertex("fifthVertex"));
        undirectedGraph.addVertex(new Vertex("sixVertex"));


        undirectedGraph.addEdge(new Edge(),undirectedGraph.getVertexByName("firstVertex"),undirectedGraph.getVertexByName("secondVertex"));
        undirectedGraph.addEdge(new Edge(),undirectedGraph.getVertexByName("firstVertex"),undirectedGraph.getVertexByName("fourthVertex"));
        undirectedGraph.addEdge(new Edge(),undirectedGraph.getVertexByName("secondVertex"),undirectedGraph.getVertexByName("thirdVertex"));
        undirectedGraph.addEdge(new Edge(),undirectedGraph.getVertexByName("thirdVertex"),undirectedGraph.getVertexByName("fourthVertex"));
        undirectedGraph.addEdge(new Edge(),undirectedGraph.getVertexByName("fourthVertex"),undirectedGraph.getVertexByName("fifthVertex"));
        undirectedGraph.addEdge(new Edge(),undirectedGraph.getVertexByName("sixVertex"),undirectedGraph.getVertexByName("fifthVertex"));


        assertTrue(!undirectedGraph.getPath(undirectedGraph.getVertexByName("firstVertex"),undirectedGraph.getVertexByName("fifthVertex")).isEmpty());
        assertTrue(!undirectedGraph.getPath(undirectedGraph.getVertexByName("sixVertex"),undirectedGraph.getVertexByName("fifthVertex")).isEmpty());
        assertTrue(!undirectedGraph.getPath(undirectedGraph.getVertexByName("fifthVertex"),undirectedGraph.getVertexByName("sixVertex")).isEmpty());
    }
}
